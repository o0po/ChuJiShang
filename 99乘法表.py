
#for 循环正三角
for j in range(1,10):   
  for i in range(1,j+1):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
  print('')


print('='*20)

#for 循环倒三角
for j in range(9,0,-1):
  for i in range(1,j+1):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
  print('')


print('='*20)

#for 循环反正三
for j in range(1,10):
  for l in range (1,10-j): #空格对齐
    print(end='        ')
  for i in range(j,0,-1):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
  print('')
  

print('='*20)


#for 循环反倒三角
for j in range(9,0,-1):
  for l in range (1,10-j): #空格对齐
    print(end='        ')
  for i in range(j,0,-1):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
  print('')






print('-'*80)




#while 循环正三角
j=1
while(j<=9):
  i=1
  while(i<=j):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
    i+=1
  print('')
  j+=1
print('')


print('='*20)

#while 循环倒三角
j=9
while(j>0):
  i=1
  while(i<=j):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
    i+=1
  print('')
  j-=1
print('')


print('='*20)

#while 循环反正三角
j=1
while(j<=9):
  l=1
  while(l<=9-j):
    print(end='        ')
    l+=1
  i=j
  while(i>0):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
    i-=1
  print('')
  j+=1
print('')


print('='*20)

#while 循环反倒三角
j=9
while(j>0):
  l=1
  while(l<=9-j):
    print(end='        ')
    l+=1
  i=j
  while(i>0):
    print('{}*{}={:<4}'.format(i,j,j*i),end='')
    i-=1
  print('')
  j-=1
print('')



#银行账户字典列表有三个账户
account = [
	{"name":"aa","money":50,"password":"1234"},
	{"name":"bb","money":3012345093241230,"password":"1234"},
	{"name":"cc","money":-90,"password":"1234"}
]

#检验账户的函数
def login():
	global judge
	global money
	for i in account:
		if username==i["name"]:
			if userpassword==i["password"]:
				print(i['name'],"欢迎您使用本机！")
				money = i["money"]
				judge = True

#余额函数
def showmoney():
	print('|{0:<12}|{1:<8}|'.format('name','money'))
	print('='*20)
	print('|{0:<12}|{1:<8}|'.format(username,money))

#存钱的函数
def savemoney():
        global money
        showmoney()
        print("存多少钱")
        plus = int(input())
        money = money+plus
        showmoney()

#取钱的函数
def getmoney():
	global money
	showmoney()
	print("取多钱")
	outmoney = int(input())
	money = money-outmoney
	showmoney()
#显示主界面的函数
def show():	
	global judge
	login()
	while judge:
		print('='*10,'银行管理系统','='*10)
		print('{0:1} {1:13} {2:14}'.format(' ','1.余额','2.存款'))
		print('{0:1} {1:13} {2:14}'.format(' ','3.取款','4.退出'))
		key=int(input('请输入：'))

		if key==1:
			print('余额：')
			showmoney()
		elif key==2:
			print('存款：')
			savemoney()
		elif key==3:
			print('取款')
			getmoney()
		elif key==4:
			print('再见')
			break
		else:
			print('错误')
	else:
		print("错误的用户名和密码")

#运行

print("请输入您的用户名：")
username = input()
print("请输入您的密码：")
userpassword = input()
judge = False
show()
